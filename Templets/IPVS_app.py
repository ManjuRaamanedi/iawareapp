#IPVS_app
#assesment-api
#import mysql.connector
#from mysql.connector import Error,MySQLConnection
#from mysql.connector.cursor import MySQLCursorPrepared
from flask import Flask,request,render_template,jsonify
import pandas as pd
import time
#from DB.DBQ import DBconn
import random
import request
import requests
from flask import Flask
from flask import request
import json
import re
#from openpyxl import load_workbook
from flask import Flask, request, render_template, redirect, url_for
#from werkzeug.utils import secure_filename
import os
from flask import render_template, make_response
import matplotlib.pyplot as plt

 
app = Flask(__name__)

#DBQ connection
#db =DBconn()

#Login-page 
@app.route('/')
def landingPage():
        return 'Welcome Iaware App'
	#return render_template('Login.html')
'''
#Admin-Home 
@app.route('/AdminAcess',methods=['POST','GET'])
def Adminacess():
    return render_template("AdminAcess.html")

#file upload page
@app.route('/Fileuploader', methods = ['GET', 'POST'])
def UploadFile():
    return render_template("FileUpload.html")

#user-home 
@app.route('/Assessment-Home')
def userDahsboard():
	return render_template('User-home.html')

#get Userlogin page           
@app.route('/loginVerify',methods=['POST','GET'])
def Userloginverify():
    username=request.form['Username']
    password=request.form['Password']
    Result=db.Usergetlogin(username,password)
    # print(Result)
    return Result
    
#only user can register 
@app.route('/Register',methods=['POST','GET'])
def userregister():
    return render_template("UserReagister.html")

#only Admin can register 
@app.route('/AdminRegister',methods=['POST','GET'])
def AdminRegister():
    return render_template("AdminReagister.html")

#User-registration
@app.route('/Doregistration', methods=['POST','GET'])
def do_registration():
       UserID=random.randint(100000,999999)
       FirstName=request.form['FirstName']
       LastName=request.form['LastName']
       Email=request.form['Email']
       Password=request.form['Password']
       RoleType="User"
       try:
           result =db.UserRegister(UserID,FirstName,LastName,Email,Password,RoleType)
           return result
       except Exception as e:
           return e

#DoAdminregistration-registration
@app.route('/DoAdminregistration', methods=['POST','GET'])
def do_Adminregistration():
       UserID=random.randint(100000,999999)
       FirstName=request.form['FirstName']
       LastName=request.form['LastName']
       Email=request.form['Email']
       Password=request.form['Password']
       RoleType="Admin"
       try:
           result =db.UserRegister(UserID,FirstName,LastName,Email,Password,RoleType)
           return result
       except Exception as e:
           return e

#file uploding
@app.route('/Datauploader', methods = ['GET', 'POST'])
def FileDataupload():
    UPLOAD_FOLDER='/home/datagrazp/project/Assesment/assesment-api/Questions/'
    if request.method == 'POST':
        f = request.files['file']
        fil= secure_filename(f.filename)
        # print(fil)
        f.save(os.path.join(UPLOAD_FOLDER, fil))
        file_path="/home/datagrazp/project/Assesment/assesment-api/Questions/"+str(fil)
        # print(file_path)
        # AddTopics(file_path)
        AddQuestion(file_path)
        # AddTest(file_path)
        return "file uploded"

#uploading Qusetion Bank from excel
@app.route('/Course', methods=['POST','GET'])
def Course(filename):
    try:
        col=0
        row=0
        file = filename
        df=pd.ExcelFile(file)
        df=pd.read_excel(df,'Course')
        total_col=(df.columns)
        while True:
            #QBID
            QBID=list(df[total_col[col]])
            QBID=(QBID[row])
            # print("QBID:",QBID)
            #Course
            col=col+1
            Course=list(df[total_col[col]])
            Course=(Course[row])
            # print("Title:",Course)
            #Discription
            col=col+1
            Discription=list(df[total_col[col]])
            Discription=(Discription[row])
            # print("Discription:",Discription)
            # print('\n')	
            # print('\n')	
            row=row+1
            col=0
            db.CourseInsert(QBID,Course,Discription)
            time.sleep(2)
        return "Course Insert sucessfully"     
    except IndexError:
        print("Total Rows ,Question updted:",row)
        return "All row updated"

#reading Topics from Excel and insert
@app.route('/AddTopics',methods=['POST','GET'])
def AddTopics(filename):
    try:
        col=0
        row=0
        file = filename
        df=pd.ExcelFile(file)
        df=pd.read_excel(df,'Topic')
        total_col=(df.columns)
        # print("total_col------------------->",total_col)
        while True:
            QBID=list(df[total_col[col]])
            QBID=(QBID[row])
            col=col+1
            Course=list(df[total_col[col]])
            Course=(Course[row])
            col=col+1
            TopicID=list(df[total_col[col]])
            TopicID=(TopicID[row])
            col=col+1
            Topicname=list(df[total_col[col]])
            Topicname=(Topicname[row])
            row=row+1
            col=0
            db.TopicsInsert(QBID,Course,TopicID,Topicname)
            time.sleep(2)
            # print(QBID,Course,TopicID,Topicname)
        return "topics inserted"
    except IndexError:
        print("Total Rows ,Question updted:",row)
        return "All row updated"


#reading excel and Uploading Questions to DB
@app.route('/AddQuestion',methods=['POST','GET'])
def AddQuestion(filename):
    try:
        col=0
        row=0
        file = filename
        df=pd.ExcelFile(file)
        df=pd.read_excel(df,'Questions')
        total_col=(df.columns)
        while True:
            QBID=list(df[total_col[col]])
            QBID=(QBID[row])
            # print("QBID:",QBID)
            col=col+1
            Course=list(df[total_col[col]])
            Course=(Course[row])
            # print("Course:",Course)
            col=col+1
            TopicID=list(df[total_col[col]])
            TopicID=(TopicID[row])
            # print("TopicID:",TopicID)
            col=col+1
            Topic=list(df[total_col[col]])
            Topic=(Topic[row])
            # print("Topic:",Topic)
            col=col+1
            QuestionID=list(df[total_col[col]])
            QuestionID=(QuestionID[row])
            # print("QuestionID:",QuestionID)
            col=col+1
            Qustion=list(df[total_col[col]])
            Qustion=(Qustion[row])
            # print("Qustion:",Qustion)
            col=col+1
            Option1=list(df[total_col[col]])
            Option1=(Option1[row])
            # print("Option1:",Option1)
            col=col+1
            Option2=list(df[total_col[col]])
            Option2=(Option2[row])
            # print("Option2:",Option2)
            col=col+1
            Option3=list(df[total_col[col]])
            Option3=(Option3[row])
            # print("Option3:",Option3)
            col=col+1
            Option4=list(df[total_col[col]])
            Option4=(Option4[row])
            # print("Option4:",Option4)
            col=col+1
            Answer=list(df[total_col[col]])
            Answer=(Answer[row])
            # print("Answer:",Answer)
            # print('\n')	
            # print('\n')
            row=row+1
            col=0
            res=db.QuestionInsert(int(QBID),str(Course),str(TopicID),str(Topic),str(Qustion),str(Option1),str(Option2),str(Option3),str(Option4),str(Answer))
            # print("------------------------------",res)
            time.sleep(2)
    except IndexError:
        print("Total Rows ,Question updted:",row)
        print("All row updated")

#Uploading Tests from Excel
@app.route('/AddTest', methods=['POST','GET'])
def AddTest(filename):
    try:
        col=0
        row=0
        file = filename
        df=pd.ExcelFile(file)
        df=pd.read_excel(df,'Test')
        total_col=(df.columns)
        while True:
            #QBID
            QBID=list(df[total_col[col]])
            QBID=(QBID[row])
            # print("QBID:",QBID)
            #Test_Title
            col=col+1
            Title=list(df[total_col[col]])
            Title=(Title[row])
            # print("Title:",Title)
            #Test_discription
            col=col+1
            Discription=list(df[total_col[col]])
            Discription=(Discription[row])
            # print("Discription:",Discription)
            #QID'S
            col=col+1
            TestQuestions_id=list(df[total_col[col]])
            TestQuestions_id=(TestQuestions_id[row])
            # print("QID:",TestQuestions_id)
            # print('\n')	
            # print('\n')
            row=row+1
            col=0
            db.CreateTest(QBID,Title,Discription,TestQuestions_id)  #creating test
    except IndexError:
        print("Total Rows ,Question updted:",row)
        return "All row updated"
   
#Get Test List
@app.route('/showCourse', methods=['POST','GET']) 
def  Showtests():
    try:
        courseList=db.SelectCourse()
        return render_template("Course.html", courseTopic=courseList)  
    except Exception as e:
        return e

#Show Topics for selected cource
@app.route('/getTopics', methods=['POST','GET'])
def getTopics():
    try:
        CourseID=request.form['CourseID']
        db.getTopic(CourseID)
        return "sucess"
    except Exception as e:
        return e

#Show Topics fTopicsor selected cource
@app.route('/showTopics', methods=['POST','GET'])
def showTopics():
    try:
        res=db.showTopics()
        # print(res)
        return render_template("Topics.html",Topics=res)
    except Exception as e:
        return e 

#selecting the Questions and options based on selected  Choose Topic 
@app.route('/chooseTopic', methods=['POST','GET'])
def chooseTopic():
    try:
        Topicname=request.form['Topicname']
        # TopicID=request.form['TopicID']
        res=db.getAssesmentQue(Topicname)
        if (res=="Requested assesment is not ready"):
            return "Requested assesment is not ready"
        else:
            return "sucess"
    except Exception as e:
        return e

#Assessment for seleted topic test
@app.route('/assessment', methods=['POST','GET'])
def Assessment():
    assessmentData=db.AssesmentQues()
    return render_template("assessment.html",result=assessmentData)     
   
#SubmitAnswer for Assesment
@app.route('/SubmitAnswer', methods=['POST','GET'])
def SubmitAnswer():
    try: 
        TestData=(request.form['TestData'])
        TestData=json.loads(TestData)
        # Results_Cal
        Test_Question=[]
        Given_Answer=[]
        correct_Answer=[]
        marks=0
        TotMarks=[]
        Percentage=0
        global UserID
        global assementResult
        for AData in TestData:
            UserID=AData['UserID']
            UserID=UserID['UserID']
            QuestionID=AData['QuestionID']
            Question=AData['Question']
            givenAnswer=AData['givenAnswer']
            Cource=AData['Cource']
            Topic=AData['Topic']
            Test_Question.append(QuestionID)
            Given_Answer.append(givenAnswer)
            cAnswer=db.verifyAnswer(QuestionID)
            correctanswers=(cAnswer[0][0].decode("utf8",errors='ignore'))
            correct_Answer.append(correctanswers)
            if ((givenAnswer.lower())==(correctanswers.lower())):
                marks=marks+1
                TotMarks.clear()
                TotMarks.append(marks)
        totalQuestion=(len(Test_Question))
        if (len(TotMarks)==0):
            marks=0
            Percentage=0
            Result="Bad"
            res=db.addAssementReport(str(UserID),str(Cource),str(Topic),str(totalQuestion),str(Test_Question),str(Given_Answer),str(correct_Answer),int(marks),float(Percentage),str(Result))
            assementResult={"userID":UserID,"Cource":Cource,"Topic":Topic,"AssmentQID":Test_Question,"correct_Answer":correct_Answer,"given_answer":Given_Answer,"Total_Test_Question":totalQuestion,"total_marks":marks,"Percentage":Percentage,"Result":Result}
            return "test submitted successfully"
        else:
            marks=TotMarks[0]
            Percentage=(marks/totalQuestion)*100
            if (Percentage<=35.0):
                Result="Bad"
                res=db.addAssementReport(str(UserID),str(Cource),str(Topic),str(totalQuestion),str(Test_Question),str(Given_Answer),str(correct_Answer),int(marks),float(Percentage),str(Result))
                assementResult={"userID":UserID,"Cource":Cource,"Topic":Topic,"AssmentQID":Test_Question,"correct_Answer":correct_Answer,"given_answer":Given_Answer,"Total_Test_Question":totalQuestion,"total_marks":marks,"Percentage":Percentage,"Result":Result}
            elif(Percentage>35.0):
                Result="Average"
                res=db.addAssementReport(str(UserID),str(Cource),str(Topic),str(totalQuestion),str(Test_Question),str(Given_Answer),str(correct_Answer),int(marks),float(Percentage),str(Result))
                assementResult={"userID":UserID,"Cource":Cource,"Topic":Topic,"AssmentQID":Test_Question,"correct_Answer":correct_Answer,"given_answer":Given_Answer,"Total_Test_Question":totalQuestion,"total_marks":marks,"Percentage":Percentage,"Result":Result}
            elif (Percentage<=50.0 or Percentage>=80.0):
                Result="Good"
                res=db.addAssementReport(str(UserID),str(Cource),str(Topic),str(totalQuestion),str(Test_Question),str(Given_Answer),str(correct_Answer),int(marks),float(Percentage),str(Result))
                assementResult={"userID":UserID,"Cource":Cource,"Topic":Topic,"AssmentQID":Test_Question,"correct_Answer":correct_Answer,"given_answer":Given_Answer,"Total_Test_Question":totalQuestion,"total_marks":marks,"Percentage":Percentage,"Result":Result}
            elif(Percentage>=80.0):
                Result="Very Good"
                res=db.addAssementReport(str(UserID),str(Cource),str(Topic),str(totalQuestion),str(Test_Question),str(Given_Answer),str(correct_Answer),int(marks),float(Percentage),str(Result))
                assementResult={"userID":UserID,"Cource":Cource,"Topic":Topic,"AssmentQID":Test_Question,"correct_Answer":correct_Answer,"given_answer":Given_Answer,"Total_Test_Question":totalQuestion,"total_marks":marks,"Percentage":Percentage,"Result":Result}
        return "test submitted successfully"
    except Exception as e :
        return (e)

#show Final result
@app.route('/Result', methods=['POST','GET'])
def FinalResult():
    try:
        result=assementResult
        AssmentQID=result["AssmentQID"]
        GivenAnswers=result["given_answer"]
        correctAnswers=result["correct_Answer"]
        Total_Test_Question=result["Total_Test_Question"]
        total_marks=result["total_marks"]
        Percentage=result["Percentage"]
        Cource=result["Cource"]
        Topic=result["Topic"]
        Result=result["Result"]
        Questions=[]
        Option1=[]
        Option2=[]
        Option3=[]
        Option4=[]
        resource=[]
        data={}
        for i in AssmentQID:
            result=db.Questions(i)
            result1=(result[0][0].decode("utf8",errors='ignore'))
            result2=(result[0][1].decode("utf8",errors='ignore'))
            result3=(result[0][2].decode("utf8",errors='ignore'))
            result4=(result[0][3].decode("utf8",errors='ignore'))
            result5=(result[0][4].decode("utf8",errors='ignore'))
            Questions.append(result1)
            Option1.append(result2)
            Option2.append(result3)
            Option3.append(result4)
            Option4.append(result5)
        for index in range(len(AssmentQID)):
            data= {"Questions":Questions[index],"Option1":Option1[index],"Option2":Option2[index],"Option3":Option3[index],"Option4":Option4[index],"GivenAnswers":GivenAnswers[index],"correctAnswers":correctAnswers[index]}
            resource.append(data)
        res={"Cource":Cource,"Topic":Topic,"Total_Test_Question":Total_Test_Question,"total_marks":total_marks,"Percentage":Percentage,"Result":Result}
        # print("---------->",res)
        return render_template("Result.html",test=resource,result=res)
        # return "assesment results"
    except Exception as e :
        return (e)

#get report based on userID

def SendReport():
    try:
        emailID=request.form['emailID']
        result=db.RquestedResult(emailID)
        return jsonify(result)
    except Exception as e :
        return (e)    

# #showreport
@app.route('/ShowReport', methods=['POST','GET'])
def ShowReport():
    finalReport=db.reqReport()
    print(finalReport)
    return render_template("Report.html",report=finalReport)
'''
if __name__ == '__main__':
        app.debug = True
        app.run(host = '0.0.0.0',port=5000)

        

